import java.util.Random;

public class RouletteWheel {
    private Random r;
    private int number;

    public RouletteWheel(){
        r= new Random();
        this.number=0;

    }
    public void spin(){
       this.number = r.nextInt(37);
    }

    public int getValue(){
        return this.number;
    }

    }

