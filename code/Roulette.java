import java.util.Scanner;
public class Roulette {

    Scanner sc = new Scanner(System.in);
    private int funds = 1000;
    RouletteWheel wheel = new RouletteWheel();
    int totallosings;
    int totalwinnings;

    public int getBet (Scanner sc , int funds){
        int betAmount =0;
        System.out.println("Would you like to place a bet? (y/n)");
        String betoption = sc.nextLine();
            if(betoption.equalsIgnoreCase("y")){
            
         while (true){
            System.out.println("Enter Your bet... You have $" + funds);
            betAmount = sc.nextInt();
            if(betAmount <= funds){
                return betAmount;
            }
            else{
            System.out.println("You dont have enough money for that bet...");
            }
         }
        }
        else {
            System.out.println("Thanks for playing...");
            System.exit(0);
        }
            return betAmount;
    }
        public int BetNumber(){
            System.out.println("What's the number you'd like to bet on?(0-36)?");
            int betNumber = sc.nextInt();
            return betNumber;
        }
        public boolean checkWinner(int spinResult, int betNumber){
            if (spinResult == betNumber){
                return true;
            }
            else{
                return false;
            }
        }

        public void playRound() {
            int betAmount = getBet(sc, funds);
            
            int betNumber = BetNumber();

            wheel.spin();
            int winningNumber = wheel.getValue();
            System.out.println("The Winning number is: " + winningNumber);

            if(winningNumber == 0){
                System.out.println("Sorry, you lost $" + betAmount);
                     totallosings =+ betAmount;
                    funds-=betAmount;
            } 
            else if(winningNumber==betNumber){
                    int winnings = betAmount *35;
                    funds += winnings;
                    System.out.println("Nicely Done, you won $"+ winnings) ;
                    totalwinnings += winnings;
            }
            else{
                funds -= betAmount;
                System.out.println("Better luck next time, you lost $" + betAmount);
                totallosings += betAmount;

            }
            sc.nextLine();
            System.out.println("Your balance is now $" + funds);
        }

    public static void main(String[] args){
        System.out.println("Welcome to my Roulette game!");
        Roulette game = new Roulette();

        boolean keepPlaying=true;
        while(keepPlaying){
            game.playRound();
                if (game.funds <= 0){
                    keepPlaying = false;
                    System.out.println("Seems like you ran out of money...");
                }
                else {
                    System.out.println("Would you like to play again? (y/n)");
                    String again = game.sc.nextLine();
                    if(again.equalsIgnoreCase("n")){
                        keepPlaying=false;
                    }

                }
        }
        System.out.println("Thanks for playing!");
        System.out.println("Your final balance is $" +game.funds);
        System.out.println("You lost $"+ game.totallosings);
        System.out.println("You won $" + game.totalwinnings);
    }
}